#ifndef PICO_QUEUE_CXX_H
#define PICO_QUEUE_CXX_H

#include <type_traits>

#include "pico/util/queue.h"

namespace picoqueue {

template <typename T>
class Queue {
    static_assert(std::is_copy_constructible<T>());

public:
    explicit Queue(const uint count) {
        queue_init(&_queue, sizeof(T), count);
    }

    Queue(const uint count, const uint spinlock_num) {
        queue_init_with_spinlock(&_queue, sizeof(T), count, spinlock_num);
    }

    Queue(const Queue &) = delete;

    Queue &operator=(const Queue &) = delete;

    ~Queue() {
        queue_free(&_queue);
    }

    bool try_push(const T &element) {
        return queue_try_add(&_queue, &element);
    }

    void push_blocking(const T &element) {
        queue_add_blocking(&_queue, &element);
    }

    T pop_or_fail() {
        T element;
        if (!queue_try_remove(&_queue, &element)) {
            panic("Cannot pop from empty queue");
        }

        return element;
    }

    T pop_blocking() {
        T element;
        queue_remove_blocking(&_queue, &element);
        return element;
    }

    T peek_or_fail() {
        T element;
        if (!queue_try_peek(&_queue, &element)) {
            panic("Cannot peek in empty queue");
        }

        return element;
    }

    T peek_blocking() {
        T element;
        queue_peek_blocking(&_queue, &element);
        return element;
    }

    bool is_empty() {
        return queue_is_empty(&_queue);
    }

    bool is_full() {
        return queue_is_full(&_queue);
    }

    uint get_num_entries() {
        return queue_get_level(&_queue);
    }

    uint get_num_entries_unsafe() {
        return queue_get_level_unsafe(&_queue);
    }

private:
    queue_t _queue{};
};

}  // namespace picoqueue

#endif  // PICO_QUEUE_CXX_H
